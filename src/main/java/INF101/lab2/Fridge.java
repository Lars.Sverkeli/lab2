package INF101.lab2;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    public ArrayList<FridgeItem> items;
    public int size;
    
    public Fridge() {
        this.items = new ArrayList<FridgeItem>();
        this.size = 20;
    }

    public int nItemsInFridge(){
        return this.items.size();
    }

    public int totalSize(){
        return this.size;
    }

    public boolean placeIn(FridgeItem item){
        if (this.items.size() < this.size){
            this.items.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    public void takeOut(FridgeItem item){
        if (this.items.contains(item)){
            this.items.remove(item);
            return;
        }
        else {
            throw new NoSuchElementException();
        }
    }

    public void emptyFridge(){
        this.items = new ArrayList<FridgeItem>();
    }

    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expired = new ArrayList<>();
        for(FridgeItem item : this.items){
            if (item.hasExpired()) {
                expired.add(item);
            }
        }
        for(FridgeItem item : expired){
            this.items.remove(item);
        }
        return expired;
    }

}
